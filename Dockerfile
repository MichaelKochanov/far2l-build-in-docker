FROM registry.gitlab.com/michaelkochanov/far2l-build-in-docker/build-image:latest as builder

ENV DEBIAN_FRONTEND noninteractive

RUN git clone https://github.com/elfmz/far2l \
  && cd far2l \
  && commit=$(git show --summary --quiet | grep commit) \
  && version=$(cat packaging/version) \
  && version=$version \
  && shortCommit=`echo | awk '{ print substr("'"${commit}"'",8,8)}'` \
  && echo $version-$shortCommit > commit-version.txt \
  && ls -l \
  && cat commit-version.txt \
  && wget --no-check-certificate https://raw.githubusercontent.com/unxed/far2l-deb/master/portable/tty_tweaks.patch \
  && git apply tty_tweaks.patch \
  && mkdir _build \
  && cd _build \
  && cmake -DUSEWX=no -DLEGACY=no -DCMAKE_BUILD_TYPE=Release .. \
  && cmake --build . -j$(nproc --all) \
  && make install \
  && cd install/ \
  && wget https://github.com/unxed/far2l-deb/raw/master/portable/autonomizer.sh \
  && chmod +x autonomizer.sh \
  && ./autonomizer.sh \
  && rm lib/libc.so.6 \
  && rm lib/libdl.so.2 \
  && rm lib/libgcc_s.so.1 \
  && rm lib/libm.so.6 \
  && rm lib/libpthread.so.0 \
  && rm lib/libstdc++.so.6 \
  && rm lib/libresolv.so.2 \
  && rm lib/librt.so.1 \
  && cd .. \
  && mv install far2l_portable \
  && git clone https://github.com/megastep/makeself.git \
  && makeself/makeself.sh far2l_portable far2l_portable.run far2l ./far2l

FROM scratch
COPY --from=builder /far2l/_build/far2l_portable.run /far2l_portable.run
COPY --from=builder /far2l/commit-version.txt /commit-version.txt